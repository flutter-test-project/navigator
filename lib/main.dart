import 'package:flutter/material.dart';
import 'package:navigator/begin_page.dart';
import 'package:navigator/question_page.dart';
import 'package:navigator/result_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/': (context) => const BeginPage(),
        '/question': (context) => const QuestionPage(),
        '/result': (context) => const ResultPage(),
      },
      //home: const BeginPage(),
    );
  }
}
