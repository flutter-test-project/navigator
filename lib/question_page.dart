import 'package:flutter/material.dart';

class QuestionPage extends StatelessWidget {
  const QuestionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Question'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text("What is the shape of the earth ?"),
            MyButton('circle',
                onPressed: () =>
                    Navigator.pushNamed(context, '/result', arguments: false)),
            MyButton('square',
                onPressed: () =>
                    Navigator.pushNamed(context, '/result', arguments: false)),
            MyButton('triangle',
                onPressed: () =>
                    Navigator.pushNamed(context, '/result', arguments: false)),
            MyButton(
              'ball',
              onPressed: () =>
                  Navigator.pushNamed(context, '/result', arguments: true),
            ),
          ],
        ),
      ),
    );
  }
}

class MyButton extends StatelessWidget {
  final String text;
  final VoidCallback? onPressed;

  const MyButton(
    this.text, {
    super.key,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(text),
    );
  }
}
