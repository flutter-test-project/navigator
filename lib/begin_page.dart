import 'package:flutter/material.dart';

class BeginPage extends StatelessWidget {
  const BeginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Begin'),
      ),
      body: Center(
        child: TextButton(
          child: const Text('Begin'),
          onPressed: () {
            Navigator.pushNamed(context, '/question');
          },
        ),
      ),
    );
  }
}
