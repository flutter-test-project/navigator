import 'package:flutter/material.dart';

class ResultPage extends StatelessWidget {
  const ResultPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool arguments = ModalRoute.of(context)!.settings.arguments as bool;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Result'),
      ),
      backgroundColor: arguments == true ? Colors.green[100] : Colors.red[100],
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(arguments.toString()),
            TextButton(
              onPressed: () {
                Navigator.restorablePushNamedAndRemoveUntil(
                    context, '/', (route) => false);
              },
              child: const Text('End'),
            ),
          ],
        ),
      ),
    );
  }
}
