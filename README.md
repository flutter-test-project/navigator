# 🍪  🎀  𝓃𝒶𝓋𝒾𝑔𝒶𝓉💍𝓇  🎀  🍪



<svg height="40" width="40" style="fill: #02569B" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Flutter</title><path d="M14.314 0L2.3 12 6 15.7 21.684.013h-7.357zm.014 11.072L7.857 17.53l6.47 6.47H21.7l-6.46-6.468 6.46-6.46h-7.37z"/></svg><svg height="40" width="40" style="fill: #0175C2" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Dart</title><path d="M4.105 4.105S9.158 1.58 11.684.316a3.079 3.079 0 0 1 1.481-.315c.766.047 1.677.788 1.677.788L24 9.948v9.789h-4.263V24H9.789l-9-9C.303 14.5 0 13.795 0 13.105c0-.319.18-.818.316-1.105l3.789-7.895zm.679.679v11.787c.002.543.021 1.024.498 1.508L10.204 23h8.533v-4.263L4.784 4.784zm12.055-.678c-.899-.896-1.809-1.78-2.74-2.643-.302-.267-.567-.468-1.07-.462-.37.014-.87.195-.87.195L6.341 4.105l10.498.001z"/></svg>

𝙐𝙨𝙖𝙜𝙚 𝙚𝙭𝙖𝙢𝙥𝙡𝙚 𝙣𝙖𝙫𝙞𝙜𝙖𝙩𝙤𝙧 1.0




# View app 
<p align="center"><img src="./screens/1.png" width="30%" style="margin-right: 10px;"><img src="./screens/2.png" width="30%" style="margin-right: 10px;"><img src="./screens/3.png" width="30%"></p>



## routes

```shell
{
        '/': (context) => const BeginPage(),
        '/question': (context) => const QuestionPage(),
        '/result': (context) => const ResultPage(),
}
```

## pushNamed
go BeginPage to QuestionPage

```shell
Navigator.pushNamed(context, '/question');
```

## pushNamed with arguments

go QuestionPage to ResultPage wich parameters

```shell
Navigator.pushNamed(context, '/result', arguments: true)
```
<p>or</p>

```shell
Navigator.pushNamed(context, '/result', arguments: false)
```

## Get the value from arguments QuestionPage

```shell
ModalRoute.of(context)!.settings.arguments
```

## Go back to the beginning with the removal of all previous routes

```shell
Navigator.restorablePushNamedAndRemoveUntil(context, '/', (route) => false)
```

## Links

- [Navigator class](https://api.flutter.dev/flutter/widgets/Navigator-class.html)
- [Pass arguments to a named route](https://docs.flutter.dev/cookbook/navigation/navigate-with-argumentsk)

